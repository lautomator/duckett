// The entire application is wrapped in
// one variable.

var color_mixer = function (colors, targets) {

// Returns a new color.

// The new possible resulting colors:

    var org = 'orange',
        vlt = 'violet',
        grn = 'green',

// Verify the correct game params exist.

        check_params = function (opts) {

            var err = '';

            if (!opts.c1) {
                err += ' missing red';
            }
            if (!opts.c2) {
                err += ' missing yellow';
            }
            if (!opts.c3) {
                err += ' missing blue';
            }
            if (err) {
                throw 'Invalid game options: ' + err;
            }
        },

// Mix some colors.

        mix_colors = function () {

            var clr1 = targets.mix_color_one.value,
                clr2 = targets.mix_color_two.value,
                tgt = targets.result,
                color = '',
                new_color = '',
                p = document.createElement("p");

            if ((clr1 === colors.c1 && clr2 === colors.c2) ||
                    (clr1 === colors.c2 && clr2 === colors.c1)) {
                color = org;
            } else if ((clr1 === colors.c1 && clr2 === colors.c3) ||
                    (clr1 === colors.c3 && clr2 === colors.c1)) {
                color = vlt;
            } else if ((clr1 === colors.c2 && clr2 === colors.c3) ||
                    (clr1 === colors.c3 && clr2 === colors.c2)) {
                color = grn;
            } else if (clr1 === clr2) {
                color = clr1;
            } else {
                throw 'Invalid color options.';
            }

            new_color = document.createTextNode(color);

            p.appendChild(new_color);

            tgt.appendChild(p);

            return false;
        };

    check_params(colors);

    targets.mix.onsubmit = mix_colors;

};
