var color_params = {

// The available colors:
    c1: 'red',
    c2: 'yellow',
    c3: 'blue'
};

// The entire application is wrapped in
// one variable.

var color_mixer = (function (colors) {

// Returns a new color.

// The new possible resulting colors:

    var c1_2 = 'orange',
        c1_3 = 'violet',
        c2_3 = 'green',

// Verify the correct game params exist.

        check_params = function (opts) {

            var err = '';

            if (!opts.c1) {
                err += ' missing red';
            }
            if (!opts.c2) {
                err += ' missing yellow';
            }
            if (!opts.c3) {
                err += ' missing blue';
            }
            if (err) {
                throw 'Invalid game options: ' + err;
            }
        },

// Mix some colors.

        mix_colors = function (clr1, clr2) {

            var color = '';

            if ((clr1 === colors.c1 && clr2 === colors.c2) ||
                    (clr1 === colors.c2 && clr2 === colors.c1)) {
                color = c1_2;
            } else if ((clr1 === colors.c1 && clr2 === colors.c3) ||
                    (clr1 === colors.c3 && clr2 === colors.c1)) {
                color = c1_3;
            } else if ((clr1 === colors.c2 && clr2 === colors.c3) ||
                    (clr1 === colors.c3 && clr2 === colors.c2)) {
                color = c2_3;
            } else if (clr1 === clr2) {
                color = clr1;
            } else {
                throw 'Invalid color options.';
            }

            return color;
        };

    check_params(colors);

    console.log(mix_colors(colors.c2, colors.c3));

}(color_params));
