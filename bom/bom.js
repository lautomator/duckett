/*  This script returns some informaton
    about the window . */

var windowStats = {
    // the height
    h: window.innerHeight,
    hStats: document.getElementById('hStats'),
    evalH: function () {
        if (this.h < 601) {
            this.getH = "<p>" + this.h + "</p>";
        } else {
            this.getH = "<p>The window is higher than 600px: " + this.h;
        }
        return this.getH;
    },
    // the width
    w: window.innerWidth,
    wStats: document.getElementById('wStats'),
    evalW: function () {
        if (this.w < 601) {
            this.getW = "<p>" + this.w + "</p>";
        } else {
            this.getW = "<p>The window is wider than 600px: " + this.w;
        }
        return this.getW;
    },
    // history
    hist: window.history.length,
    wHist: document.getElementById('wHist'),
    // location
    loc: window.location,
    wLoc: document.getElementById('loc')
};

windowStats.wStats.innerHTML = windowStats.evalW();
windowStats.hStats.innerHTML = windowStats.evalH();
windowStats.wHist.innerHTML = windowStats.hist;
windowStats.wLoc.innerHTML = windowStats.loc;
