$(function() {

    // basic example
    $(':header').addClass('headline');
    $('li:lt(3)').hide().fadeIn(1500);

    $('li').on('click', function() {
        $(this).remove();
    });

    // looping
    $('li em').addClass('seasonal');
    $('li.hot').addClass('favorite'); // implicit iteration

    // chaining
    $('li[id!="one"]')
        .hide()
        .delay(500)
        .fadeIn(1400);

    // get some html or text and print to the console
    var listHTML = $('ul').html(),
        listText = $('ul').text(),
        listItemHTML = $('li').html(),
        listItemText = $('li').text();

    console.log(listHTML);
    console.log(listText);
    console.log(listItemHTML);
    console.log(listItemText);

    // using a function to update content
    $(function() {
        $('li:contains("pine")').text('almonds');
        $('li.hot').html(function() {
            return '<em>' + $(this).text() + '</em>';
        });
        $('li#one').remove();
    });

    // inserting new content
    $(function() {
        var newListItem = $('<li><em>gluten-free</em> soy sauce</li>');

        $('ul').before('<p class="notice">Just updated</p>');
        $('li.hot').prepend('+ ');
        $('li:last').after(newListItem);
    });

    // getting and setting attribute values
    // getting
    var attributes = $('li#one').attr('class');
    console.log("li#one's attributes:" + attributes);

    // setting
    $(function() {
        $('li#three').removeClass('hot');
        $('li.hot').addClass('favorite');
        $('ul').attr('id', 'group');
    });

    // get the HTML for the entire body, as a check for the above
    var getPageHTML = $('#page').html();
    console.log(getPageHTML);

    // getting and setting CSS properties
    // getting
    var backgroundColor = $('li').css('background-color');
    console.log(backgroundColor);
    // setting
    $('li').css('background-color', '#272727');

    // setting multiple properties with object literal notation
    $('li').css({
        'background-color': '#272727',
        'font-family': 'Courier'
    });

    // these will override the previous changes
    $(function() {
        var backgroundColor = $('li').css('background-color');
        $('ul').append('<p>Color was:' + backgroundColor + '</p>');
        $('li').css({
            'background-color': '#c5a996',
            'border': '1px solid #fff',
            'color': '#000',
            'font-family': 'Georgia',
            'padding-left': '+=75'
        });
    });
    // It's better to change the CSS in a style sheet, then in the JS
    // So the above is not advisable unless you are transforming the CSS
    // in, say, an animation or something.

    // each()
    // each() is kind of like a loop, whereby you can make changes to
    // each of the elements in a selction from a returned selector.
    // You can access a current element using this or $(this)

    $(function(){
        $('li').each(function() {
            var ids = this.id;
            $(this).append(' <span class="order">' + ids + '</span>');
            console.log(ids);
        });
    });

    // Events
    // .on() is used to handle all events
    $(function() {
        var ids = '',
            $listItems = $('li');

        $listItems.on('mouseover click', function() {
            ids = this.id;
            $listItems.children('span').remove();
            $(this).append(' <span class="priority">' + ids + '</span>');
        });

        $listItems.on('mouseout', function() {
            $(this).children('span').remove();
        });
    });


}); // IIFE ends
