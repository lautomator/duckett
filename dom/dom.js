/*

// using the Id
var el = document.getElementById('two');
el.className = 'complete';

// using the class name (to select multiple elements)
var els = document.getElementsByClassName('hot');

if (els.length > 1) {
    var el = els[els.length -1]; // change the last one
    el.className = 'cool';
}

// using css selectors (first match only)
var el = document.querySelector('li.hot');
el.className = 'cool';


// using css selectors (returns a node list)
var els = document.querySelectorAll('li.hot');
console.log(els);

// els[1].className = 'cool';
for (key in els) {
	console.log(els[key]);
	els[key].className = 'cool';
}
*/

// get the value of a node
var el = document.getElementById('two');
console.log('initial class assignment:', el.className);

var elText = el.firstChild.nodeValue;
console.log('initial text value:', elText);

// replace the value
elText = elText.replace('pine nuts', 'kale');
el.firstChild.nodeValue = elText;
console.log('new text value:', elText);

// change the class to 'complete'
el.className = 'complete';
console.log('final class assignment:', el.className);

// change the textContent of one of the items
var newTitle = document.getElementById('title');
newTitle.textContent = 'grocery list'
console.log(newTitle);

// replacing elements: innerHTML method (discouraged)
// Let's replace the old list with a new one.
var newItem = '<li id="five" class="cool">lettuce</li>';
var newEl = document.getElementById('list');
newEl.innerHTML = newItem;
