var hotel = {
    customer: '',
    rooms: 0,
    booked: false,
    feedback: '',
    processEntry: function () {
        this.customer = document.getElementById('in_name').value;
        this.rooms = document.getElementById('in_rooms').value;
        this.feedback = document.getElementById('feedback');
        this.displayName = document.getElementById('name');
        this.displayRooms = document.getElementById('rooms');

        // make sure user enters their name
        if (this.customer === '') {
            this.feedback.textContent = "-->Enter a valid name";
        } else {
            this.feedback.textContent = "";
        }

        this.displayName.textContent = this.customer;
        this.displayRooms.textContent = this.rooms;
    }
};

document.getElementsByTagName("form")[0].onsubmit = hotel.processEntry;
