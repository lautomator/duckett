(function(N) {
    var target;
    var skillLevel;
    var randomNumber;
    var guesses = 0;
    var playerFeedback;

    function validateOpts(opts) {
        var err = "";
        if (!opts.skillLevel) {
            err += " missing skill level.";
        }
        if (!opts.target) {
            err += " missing target";
        }
        if (!opts.playerFeedback) {
            err += " missing player feedback";
        }
        if (err) {
            throw "Invalid game configuration: " + err;
        }
    }

    function handleFormSubmit() {
        guesses++;
        var answer = document.getElementById('player_guess').value;
        answer = parseInt(answer);
        var msg = ""
        if (isNaN(answer)) {
            msg = "Bad input";
        } else if (answer == randomNumber) {
            msg = "WIN! in " + guesses + " tries!";
        } else {
            msg = guesses + " guesses.  Guess again.";
        }
        var p = document.createElement("p");
        var tn = document.createTextNode(msg);
        p.appendChild(tn);
        playerFeedback.appendChild(p);
        return false;
    }

    N.Game = function(opts) {
        validateOpts(opts);
        target = opts.target;
        skillLevel = opts.skillLevel;
        playerFeedback = opts.playerFeedback;

        this.init = function init() {
            // skill level 1 = 1 out of 10 chance,
            // 2 = 1 out of 100..etc
            randomNumber = Math.floor(
                Math.random() * Math.pow(10, skillLevel));
            console.log("game init-ed. the answer is: " + randomNumber);
            target.onsubmit = handleFormSubmit;
        };
    };
}(window));