var setSolution = function () {
    // Generate a solution: 3 unique random numbers.
    var randomnumber, found = false, solutionsArr = [], i;

    while (solutionsArr.length < 3) {
        randomnumber = Math.floor(Math.random() * 10);
        found = false;

        for (i = 0; i < solutionsArr.length; i += 1) {
            if (solutionsArr[i] === randomnumber) {
                found = true;
                break;
            }
        }
        if (!found) {
            solutionsArr[solutionsArr.length] = randomnumber;
        }
    }
    // join the list into a string
    return solutionsArr.join('');
};

var solution = setSolution();

var game = {
    guess: '',
    feedback: '',
    flag: '',
    para: '',
    badGuess: '',
    element: '',
    processEntry: function () {
        this.guess = document.getElementById('player_guess').value;
        this.feedback = document.getElementById('player_feedback');
        this.flag = document.getElementById('player_errors');
        this.para = document.createElement("p");
        this.badGuess = document.createTextNode(this.guess);
        this.para.appendChild(this.badGuess);
        this.element = document.getElementById("player_feedback");

        // A guess must be three numbers and not empty.
        if (!this.guess.match(/[0-9]{3}/) || this.guess === '') {
            this.flag.textContent = 'Enter numbers (0-9) only.';
            document.getElementsByTagName('form')[0].reset();
        } else if (this.guess === solution) {
            this.flag.textContent = 'You guessed it!';
            document.getElementsByTagName('form')[0].reset();
        } else {
            this.flag.textContent = '';
            this.element.appendChild(this.para);
            document.getElementsByTagName('form')[0].reset();
        }
    }
};

/* for debugging : delete later ========== */
console.log('solution: ' + solution);
var sol = document.getElementById('debug');
sol.textContent = solution;
/* ======================================= */

document.getElementsByTagName('form')[0].onsubmit = game.processEntry;
