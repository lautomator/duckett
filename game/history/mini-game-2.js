/*
    ---------
    mini-game
    ---------
    This is a preliminary study
    for the JavaScript version
    of CodeBreaker.
*/

// The game object prototype:
function Game(gameParams) {
    var skillLevel = gameParams.skillLevel,
        players = gameParams.players,
        playerTarget = gameParams.playerTarget,
        playerFeedback = gameParams.playerFeedback,
        playerGuess = gameParams.playerGuess;

    // Validate the game parameters.
    function validateParams(params) {
        var err = '';

        if (!params.skillLevel) {
            err += " missing skill level";
        }
        if (!params.players) {
            err += " missing number of players";
        }
        if (!params.playerTarget) {
            err += " missing target";
        }
        if (!params.playerFeedback) {
            err += " missing player feedback";
        }
        if (err) {
            throw "Invalid game configuration: " + err;
        }
    }

    // method to return the solution of a game:
    function setSolution(skill) {
        // Generate a solution: n=skillLevel unique random numbers.
        var randomnumber, found = false, solutions = [], i;
        this.skill = skill;

        while (solutions.length < this.skill) {
            randomnumber = Math.floor(Math.random() * 10);
            found = false;

            for (i = 0; i < solutions.length; i += 1) {
                if (solutions[i] === randomnumber) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                solutions[solutions.length] = randomnumber;
            }
        }
        // Join the list into a string.
        return solutions.join('');
    }

    // method to evaluate a guess:
    function processGuess() {
        var guess = document.getElementById('player_guess').value,
            feedback = document.getElementById('player_feedback'),
            response = '',
            status = '',
            p = document.createElement("p");

        // A guess must be n=skillLevel numbers and must not be empty.
        if (!guess.match(/[0-9]{3,}/) || guess === '') {
            response = 'Enter numbers (0-9) only.';
        } else if (guess === solution) {
            response = 'You guessed it!';
        } else {
            response = guess;
        }
        status = document.createTextNode(response);
        p.appendChild(status);
        feedback.appendChild(p);



        return false;
    }

    this.init = function () {

        var solution = setSolution(skillLevel);

        validateParams(gameParams);
        playerTarget.onsubmit = processGuess;

        // for debugging -----------------------
        console.log(solution);
        // -------------------------------------

    };
}
