/*
    ---------
    mini-game
    ---------
    This is a preliminary study
    for the JavaScript version
    of CodeBreaker.
*/

// The game object constructor:
function Game(skillLevel, players) {
    this.skillLevel = skillLevel;
    this.players = players;

    // method to return the solution of a game:
    this.setSolution = function () {
        // Generate a solution: n=skillLevel unique random numbers.
        var randomnumber, found = false, solutions = [], i;

        while (solutions.length < this.skillLevel) {
            randomnumber = Math.floor(Math.random() * 10);
            found = false;

            for (i = 0; i < solutions.length; i += 1) {
                if (solutions[i] === randomnumber) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                solutions[solutions.length] = randomnumber;
            }
        }
        // Join the list into a string.
        return solutions.join('');
    };

    // method to process a guess:
    this.processGuess = function (guess, solution) {
        this.guess = guess;
        this.solution = solution;
        var response;

        // A guess must be n=skillLevel numbers and must not be empty.
        if (!this.guess.match(/[0-9]{3,}/) || this.guess === '') {
            response = 'Enter numbers (0-9) only.';
        } else if (this.guess === this.solution) {
            response = 'You guessed it!';
        } else {
            response = this.guess;
        }
        return response;
    };
}

// Let's play!
var playGame = (function () {

    // Construct a new game object and set a solution.
    var guessGame = new Game(3, 1),
        solution = guessGame.setSolution(),
        // Evaluate a guess and display feedback to the player.
        getGuess = function () {
            var guess = document.getElementById('player_guess').value,
                input = guessGame.processGuess(guess, solution),
                feedback = document.getElementById('player_feedback'),
                p = document.createElement("p"),
                status = document.createTextNode(input);

            p.appendChild(status);
            feedback.appendChild(p);

            document.getElementsByTagName('form')[0].reset();
        };

    // for debugging -----------------------
    console.log('solution: ' + solution);
    // -------------------------------------

    document.getElementsByTagName("form")[0].onsubmit = getGuess;
}());
